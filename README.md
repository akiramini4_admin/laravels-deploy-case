# LaravelS 持续集成与部署案例

## 注意事项
在MasterPipeline.yml文件中，内置了部署阶段，如下图所示。用于将构建产出部署到远程主机上，如您不需要部署远程主机可删除此任务。如需要部署远程主机，请按照如下手册添加主机，并在yml中更改 hostGroupID。[添加主机戳这里>>>](https://gitee.com/help/categories/76)

```
- stage: 
  name: deploy
  displayName: 部署
  steps:
    - step: deploy@agent
      name: deploy_agent
      displayName: '主机部署'
      # 请按照手册添加主机https://gitee.com/help/categories/76，并更改此ID
      hostGroupID: gitee-go-test
      deployArtifact:
          # 部署包来源，build表示来源于上游流水线，artifact表示来源于制品仓库，推荐使用build
        - source: artifact
          # 将部署包下载到主机后的部署包名，默认output
          name: output
          # 将部署包下载主机的具体目录，默认 ~/gitee_go/deoloy
          target: ~/gitee_go/deoloy
          # 上游制品名，只有当source为build时生效，默认output
          dependArtifact: output
          # 以下内容只有在source为artifact时生效，制品库名，默认default
          artifactRepository: release
          # 制品名，默认output
          artifactName: output
          # 制品版本，默认取最新
          artifactVersion: latest
      script: 
        |
        cd ~/gitee_go/deoloy
        tar zxvf laravels-server.tar.gz
        cd sourcecode/ && unzip sources.zip && rm sources.zip
        composer install && cd ..
        rm -fr ~/gitee_go/deoloy/laravels
        mv ~/gitee_go/deoloy/sourcecode ~/gitee_go/deoloy/laravels
        cd ~/gitee_go/deoloy/laravels
        echo Y | php artisan laravels publish
        php bin/laravels start -d
        rm ../laravels-server.tar.gz
        echo "Finish"
```


## 帮助手册
https://gitee.com/help/categories/69
